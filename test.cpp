#include <gtest/gtest.h>
#include "verz.h"


TEST(Verz_Constructor, Default_Constructor)
{
    Verz verz;
    ASSERT_NO_THROW(verz);
    ASSERT_EQ(1, verz.get_diameter());
}

TEST(Verz_Constructor, Initial_Constructor)
{
    ASSERT_NO_THROW(Verz verz_1(2));
}

TEST(Verz_Methods, Setters) {
    Verz verz;
    ASSERT_NO_THROW(verz.change_diameter(8));
}

TEST(Verz_Methods, Getters) {
    Verz verz_1(1);
    ASSERT_EQ(1, verz_1.get_diameter());
    ASSERT_EQ(0.5, verz_1.get_ordinate(1.0));
    ASSERT_DOUBLE_EQ(-0.57735026918962584, verz_1.get_left_kink_x());
    ASSERT_DOUBLE_EQ(0.57735026918962584, verz_1.get_right_kink_x());
    ASSERT_DOUBLE_EQ(0.75, verz_1.get_kink_y());
    ASSERT_DOUBLE_EQ(3.1415926535897931, verz_1.get_square());
    ASSERT_DOUBLE_EQ(4.934802200544679, verz_1.get_volume());
    ASSERT_STREQ("y = (1.000000^3) / (1.000000^2 + x^2)", verz_1.get_equation());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
