#include <iostream>
#include <cstdlib>
#include <cmath>
#include "verz.h"
using namespace std;

Verz::Verz(double _diameter) {
  diameter = _diameter;
}
double Verz::get_diameter() {
  return diameter;
}
void Verz::change_diameter(double _diameter) {
  if (_diameter>=0) {
    diameter = _diameter;
  }
}
double Verz::get_ordinate(double x) {
  return pow(diameter, 3.0)/(pow(diameter, 2.0) + pow(x, 2.0));
}
double Verz::get_left_kink_x() {
  return -diameter/sqrt(3);
}
double Verz::get_right_kink_x() {
  return diameter/sqrt(3);
}
double Verz::get_kink_y() {
  return 3*diameter/4;
}
double Verz::get_square() {
  return M_PI*pow(diameter, 2.0);
}
double Verz::get_volume() {
  return pow(M_PI, 2.0)*pow(diameter, 3.0)/2;
}
char* Verz::get_equation() {
  char* ptr = (char*)calloc(1, 100);
  snprintf(ptr, 100, "y = (%f^3) / (%f^2 + x^2)", diameter, diameter);
  return ptr;
}
