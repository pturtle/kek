all: verz test

verz: main.cpp
	g++ main.cpp verz.cpp -o result.out

test: test.cpp
	g++ test.cpp verz.cpp -o runTests.out -lgtest -lpthread
