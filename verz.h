#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

class Verz {
private:
  double diameter;
public:
  Verz(double _diameter = 1.0);
  double get_diameter();
  void change_diameter(double _diameter);
  double get_ordinate(double x);
  double get_left_kink_x();
  double get_right_kink_x();
  double get_kink_y();
  double get_square();
  double get_volume();
  char* get_equation();
};
