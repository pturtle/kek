#include <iostream>
#include "verz.h"
using namespace std;

void print_menu() {
  cout << "1. Get current diameter" << endl;
  cout << "2. Set new diameter" << endl;
  cout << "3. Get ordinate" << endl;
  cout << "4. Get kinks" << endl;
  cout << "5. Get square between verziera and asymptote" << endl;
  cout << "6. Get volume of rotating around asymptote" << endl;
  cout << "7. Get equation" << endl;
  cout << "8. Exit" << endl;
  cout << "Input your choice>>";
}

int main() {
  int choice;
  char* temp_ptr;
  double temp;

  Verz* verz = new Verz();

  for (;;) {
    print_menu();
    cin >> choice;
    switch (choice) {
      case 1:
        cout << "Result is: " << verz->get_diameter() << endl;
        continue;
      case 2:
        cout << "Input new diameter>>";
        cin >> temp;
        verz->change_diameter(temp);
        cout << "Done." << endl;
        continue;
      case 3:
        cout << "Input x>>";
        cin >> temp;
        cout << "Result is: " << verz->get_ordinate(temp) << endl;
        continue;
      case 4:
        cout << "Left kink: (" << verz->get_left_kink_x() << ", " << verz->get_kink_y() << ")" << endl;
        cout << "Right kink: (" << verz->get_right_kink_x() << ", " << verz->get_kink_y() << ")" << endl;
        continue;
      case 5:
        cout << "Result is: " << verz->get_square() << endl;
        continue;
      case 6:
        cout << "Result is: " << verz->get_volume() << endl;
        continue;
      case 7:
        temp_ptr = verz->get_equation();
        cout << temp_ptr << endl;
        free(temp_ptr);
        continue;
      default:
        break;
    }
    break;
  }
  delete verz;
}
